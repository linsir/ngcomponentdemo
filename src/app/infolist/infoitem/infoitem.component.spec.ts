import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoitemComponent } from './infoitem.component';

describe('InfoitemComponent', () => {
  let component: InfoitemComponent;
  let fixture: ComponentFixture<InfoitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
