import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Stock} from '../../model/stock';

@Component({
  selector: 'app-infoitem',
  templateUrl: './infoitem.component.html',
  styleUrls: ['./infoitem.component.css']
})
export class InfoitemComponent implements OnInit {
  private _id: number;

  @Input()
  set id(id: number) {
    this._id = id + 1;
  }

  get id(): number {
    return this._id;
  }

  @Input() stock: Stock;
  tableStyle = {
    'font-family': 'arial, sans-serif',
    'border-collapse': 'collapse',
    width: '100%'
  };

  @Output() stockVoter = new EventEmitter<Stock>();

  tdthStyle = {
    border: '1px solid #dddddd',
    'text-align': 'left',
    padding: '8px'
  };

  header: Array<string> = [
    'id',
    'name',
    'code',
    'price',
    'previousPrice',
    'favorite',
    'modify'
  ];

  constructor() {
  }

  ngOnInit() {
  }

  beengo() {
    this.stockVoter.emit(this.stock);
  }
}
