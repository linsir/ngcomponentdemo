import {Component, OnInit} from '@angular/core';
import {Stock} from '../model/stock';

@Component({
  selector: 'app-infolist',
  templateUrl: './infolist.component.html',
  styleUrls: ['./infolist.component.css']
})
export class InfolistComponent implements OnInit {
  // public stock: Stock;
  public stocks: Array<Stock>;

  constructor() {
  }

  ngOnInit() {
    // this.stock = new Stock('Test Stock Company', 'TSC', 85, 80);
    this.stocks = [
      new Stock('Test Stock Company 1', 'TSA', 85, 40),
      new Stock('Test Stock Company 2', 'TSB', 86, 50),
      new Stock('Test Stock Company 3', 'TSC', 87, 60),
      new Stock('Test Stock Company 4', 'TSD', 88, 70)
    ];
  }

  logStock(stock) {
    console.log(stock.toString());
  }

}
